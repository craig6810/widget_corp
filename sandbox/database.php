<?php
    // 1. create a db connection
    $db_host = "localhost";
    $db_password = "password";
    $db_user = "website";
    $db_name = "linda_db";
    $db_connection = mysqli_connect($db_host, $db_user, $db_password, $db_name);

    // test if connection was succesfull
    if(mysql_errno()) {
        die("Database connection failed: ".mysqli_connect_error()." (".mysqli_connect_errno().")");
    };
?>

<?php
    // 2. perform db query
    $query = "SELECT * ";
    $query .= "FROM tblStaff";
    // $result is a
    // * resource * (holds db rows?)
    $result = mysqli_query($db_connection, $query);

    if(!$result) {
        die("The query was not ran.");
    };
?>

    <!DOCTYPE html>
    <html>
    <head>
        <title>SQL DB ACCESS</title>
    </head>
    <body>

    <ul>
        <?php
        // 3. use returned data
        while($staff = mysqli_fetch_assoc($result)) {
            // output data
            ?>
            <li><?php echo $staff["name"] ?></li>
        <?php
        }
        ?>

    </ul>

    <?php
    // release returned data
    mysqli_free_result($result);
    ?>

    </body>
    </html>

<?php
// close the db connection
mysqli_close($db_connection);
?>