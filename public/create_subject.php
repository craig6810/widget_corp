<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once ("../includes/validation_functions.php")?>
<?php
    if ($_POST["submit"]) {
        // safely escape sql with my own function
        $menu_name = mysql_prep($_POST["menu_name"]);
        // type casting int
        // forcing it to be an int
        $position = (int) $_POST["position"];
        // type casting boolean
        $visible = (int) $_POST["visible"];

        //validation
        $required_fields = array("menu_name", "position", "visible");
        validate_presences($required_fields);

        $fields_with_max_lengths = array("menu_name" => 30);
        validate_max_lengths($fields_with_max_lengths);

        if(!empty($errors)) {
            $_SESSION["errors"] = $errors;
            redirect_to("new_subject.php");
        }

        // insert query
        $query  = "INSERT INTO tblSubjects (";
        $query .= " menu_name, position, visible ";
        $query .= ") VALUES (";
        $query .= " '{$menu_name}', '{$position}', '{$visible}' ";
        $query .= ")";

        // run query
        $result = mysqli_query($db_connection, $query);

        if ($result) {
            $_SESSION ['message'] = "Subject Created.";
            redirect_to("manage_content.php");
        } else {
            $_SESSION ['message'] = "Subject createion failed.";
            redirect_to("new_subject.php");
        }

    } else {
        // This is probably a get request
        redirect_to("new_subject.php");
    }
?>

<?php
    if (isset($db_connection)) { mysqli_close($db_connection); }
?>